#include "Display.h"

Display::Display(uint8_t hardwareConfig, uint8_t numSegments, const uint8_t *segmentPinsIn, const uint8_t numDigits,
                 const uint8_t *digitPins, bool disableDecPoint) {

    this->realDisplay.begin(hardwareConfig, numDigits, digitPins, segmentPinsIn, false, false, false, disableDecPoint);
    this->realDisplay.setBrightness(0);
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, LOW);
}

void Display::setBrightness(int16_t brightness) {
    this->realDisplay.setBrightness(brightness);
}

void Display::setTime(uint32_t milsLeft) {
    String result;
    uint32_t tSeconds = milsLeft / 1000;
    static char str[8];
    uint8_t minutes = tSeconds / 60;
    uint8_t seconds = tSeconds % 60;
    sprintf(str, "%02u%02u", minutes, seconds);
    this->realDisplay.setChars(str);
}

void Display::loop() {
    if(blinking){
        bool state = millis() % 1000 > 500;
        digitalWrite(LED_BUILTIN, state ? HIGH : LOW);
        this->realDisplay.setBrightness(state ? -100 : 0);
    }
    this->realDisplay.refreshDisplay();
}

void Display::setBlinking(bool b) {
    this->blinking = b;
}
