#ifndef CLOCK_DISPLAY_H
#define CLOCK_DISPLAY_H


#include <SevSeg.h>

class Display {
public:
    Display(uint8_t hardwareConfig, uint8_t numSegments, const byte segmentPinsIn[],
            const uint8_t numDigits,const uint8_t digitPins[],bool disableDecPoint=true);
    void setBrightness(int16_t brightness);
    void setTime(uint32_t milsLeft);
    void loop();

    void setBlinking(bool b);

private:
    SevSeg realDisplay;
    bool blinking = false;
};


#endif //CLOCK_DISPLAY_H
