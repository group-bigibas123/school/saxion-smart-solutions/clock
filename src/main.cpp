#include <Arduino.h>
#include <SevSeg.h>
#include <SoftwareSerial.h>

#include "Display.h"
#include "util.h"

#define TIME_LIMIT_MINUTES 45L

#define SECONDS_PER_MINUTE 60L
#define MILS_PER_SECOND 1000L

const static uint32_t timeLimit = TIME_LIMIT_MINUTES * SECONDS_PER_MINUTE * MILS_PER_SECOND;
const static byte segmentPins[7] = {/*A*/A1,/*B*/A2,/*C*/6,/*D*/3,/*E*/2,/*F*/A4,/*G*/A5};
const static byte digitPins[4] = {A3, A0, 4, 7};


uint32_t endMilis;
Display display(COMMON_CATHODE, size(segmentPins), segmentPins, size(digitPins), digitPins);

void setup() {
	Serial.begin(9600);
	//Serial.setTimeout(1);
	endMilis = millis() + timeLimit;
}

//Serial handeling zo veel mogelijk non-blocking zodat het display kan blijven updaten
void serialEvent() {
	static byte cmd = 0;
	if (Serial.available() > 0) {
		switch ((unsigned char) Serial.peek()) {
			case '?':
				Serial.read();
				Serial.println(endMilis - millis(),10);
				break;
			case 'A':
				if(Serial.available() >= 3) {
					Serial.read();
					Serial.read();
					uint32_t secs = Serial.read();
					Serial.println(secs, 10);
					endMilis = endMilis + (secs * MILS_PER_SECOND);
				}
				break;
			case 'S':
				if(Serial.available() >= 3) {
					Serial.read();
					Serial.read();
					uint32_t secs = Serial.read();
					Serial.println(secs, 10);
					endMilis = endMilis - (secs * MILS_PER_SECOND);
				}
				break;
			case '\n':
				break;
		}

	}
}

boolean done = false;

void loop() {
	if (!done) {
		uint32_t curMilis = millis();
		if (endMilis < curMilis) {
			display.setTime(0);
			display.setBlinking(true);
			done = true;
		} else {
			uint32_t milsLeft = endMilis - curMilis;
			display.setTime(milsLeft);
		}
	}
	display.loop();
}