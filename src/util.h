#ifndef CLOCK_UTIL_H
#define CLOCK_UTIL_H
template<class T, size_t N>
constexpr size_t size(T (&)[N]) { return N; }
#endif //CLOCK_UTIL_H
